function ll --wraps=ls --wraps='clear && exa -laxF --icons --no-user --no-permissions --no-time --no-filesize --sort=size --sort=type --sort=extension' --description 'alias ll=clear && exa -laxF --icons --no-user --no-permissions --no-time --no-filesize --sort=size --sort=type --sort=extension'
  clear && exa -laxF --icons --no-user --no-permissions --no-time --no-filesize --sort=size --sort=type --sort=extension $argv; 
end
