function hib --wraps='systemctl hibernate' --description 'alias hib=systemctl hibernate'
  systemctl hibernate $argv; 
end
