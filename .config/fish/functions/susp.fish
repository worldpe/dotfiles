function susp --wraps='systemctl suspend' --description 'alias susp=systemctl suspend'
  systemctl suspend $argv; 
end
