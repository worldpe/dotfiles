function night --wraps='gammastep -O 2500k -b 0.9' --description 'alias night=gammastep -O 2500k -b 0.9'
  gammastep -O 2500k -b 0.9 $argv; 
end
