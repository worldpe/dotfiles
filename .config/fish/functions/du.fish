function du --wraps='dust -n 1000' --description 'alias du=dust -n 1000'
  dust -n 1000 $argv; 
end
