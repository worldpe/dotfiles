function lt --wraps='ls -T --level=2' --description 'alias lt=ls -T --level=2'
  ls -T --level=2 $argv; 
end
