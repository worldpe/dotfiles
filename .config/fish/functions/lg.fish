function lg --wraps='ls -G' --description 'alias lg=ls -G'
  ls -G $argv; 
end
