function sle --wraps='systemctl hybrid-sleep' --description 'alias sle=systemctl hybrid-sleep'
  systemctl hybrid-sleep $argv; 
end
