function qutedefault --wraps=BROWSER=\'\'\ xdg-settings\ set\ default-web-browser\ org.qutebrowser.qutebrowser.desktop --description alias\ qutedefault=BROWSER=\'\'\ xdg-settings\ set\ default-web-browser\ org.qutebrowser.qutebrowser.desktop
  BROWSER='' xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop $argv; 
end
