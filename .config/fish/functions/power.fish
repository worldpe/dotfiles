function power --wraps='upower -i /org/freedesktop/UPower/devices/battery_BAT1' --description 'alias power=upower -i /org/freedesktop/UPower/devices/battery_BAT1'
  upower -i /org/freedesktop/UPower/devices/battery_BAT1 $argv; 
end
