function sv --wraps='sudo nvim' --wraps=sudoedit --description 'alias sv=sudoedit'
  sudoedit $argv; 
end
