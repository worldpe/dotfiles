function par --wraps=paru\ -Sl\ \|\ awk\ \'\{print\ \$2\(\$4==\"\"\ \?\ \"\"\ :\ \"\ \*\"\)\}\'\ \|\ sk\ --multi\ --preview\ \'paru\ -Si\ \{1\}\'\ \|\ cut\ -d\ \"\ \"\ -f\ 1\ \|\ xargs\ -ro\ paru\ -S --wraps=aur --description 'alias par=aur'
  aur $argv; 
end
