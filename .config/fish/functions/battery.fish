function battery --wraps='upower -i /org/freedesktop/UPower/devices/battery_BAT1' --wraps='upower -i /org/freedesktop/UPower/devices/battery_BAT1 | rg percentage:' --description 'alias battery=upower -i /org/freedesktop/UPower/devices/battery_BAT1 | rg percentage:'
  upower -i /org/freedesktop/UPower/devices/battery_BAT1 | rg percentage: $argv; 
end
