function skag --wraps=sk\ --ansi\ -i\ -c\ \'ag\ --color\ \{\}\' --description alias\ skag=sk\ --ansi\ -i\ -c\ \'ag\ --color\ \{\}\'
  sk --ansi -i -c 'ag --color {}' $argv; 
end
