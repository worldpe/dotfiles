function tr --wraps='tree -Cd' --wraps='tree -C' --description 'alias tr=tree -C'
  tree -C $argv; 
end
