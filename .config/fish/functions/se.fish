function se --wraps='set -gx' --wraps='set -Ux' --description 'alias se=set -Ux'
  set -Ux $argv; 
end
