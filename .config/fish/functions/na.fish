function na --wraps='neofetch --ascii_distro' --wraps='clear && neofetch --ascii_distro' --description 'alias na=clear && neofetch --ascii_distro'
  clear && neofetch --ascii_distro $argv; 
end
