function .m --wraps='lf ~/.config/mpd/' --description 'alias .m=lf ~/.config/mpd/'
  lf ~/.config/mpd/ $argv; 
end
