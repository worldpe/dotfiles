function pic --wraps='vimiv ~/Pictures/' --description 'alias pic=vimiv ~/Pictures/'
  vimiv ~/Pictures/ $argv; 
end
