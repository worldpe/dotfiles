function wifi-on --wraps='nmcli radio wifi on' --description 'alias wifi-on=nmcli radio wifi on'
  nmcli radio wifi on $argv; 
end
