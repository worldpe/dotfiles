function cont --wraps='podman run -it docker.io/archlinux' --description 'alias cont=podman run -it docker.io/archlinux'
  podman run -it docker.io/archlinux $argv; 
end
