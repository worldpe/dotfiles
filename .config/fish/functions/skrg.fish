function skrg --wraps=sk\ --ansi\ -i\ -c\ \'rg\ --color=always\ --line-number\ \{\}\' --description alias\ skrg=sk\ --ansi\ -i\ -c\ \'rg\ --color=always\ --line-number\ \{\}\'
  sk --ansi -i -c 'rg --color=always --line-number {}' $argv; 
end
