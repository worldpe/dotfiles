filetype plugin indent on
set ignorecase
set number
set showmatch
syntax on
set wrap
set incsearch
set hlsearch
set smartcase

call plug#begin()

" Theming
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'navarasu/onedark.nvim'

" Features
Plug 'vimwiki/vimwiki'

call plug#end()

" My settings

let mapleader = " " " Spacebar mapped to leader key

"Plug Commands
nnoremap <leader>pi <cmd>PlugInstall<cr>
nnoremap <leader>pc <cmd>PlugClean<cr>

" Writing & Quitting
nnoremap <leader>wq <cmd>wq<cr>
" nnoremap <leader>ws <cmd>w<cr>
nnoremap <leader>s <cmd>w<cr>
nnoremap <leader>q <cmd>q!<cr>

let g:onedark_style = 'darker'
colorscheme onedark

" Primeagan video
nnoremap Y y$
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" Undo break points
inoremap , ,<c-g>u
inoremap . .<c-g>u
inoremap [ [<c-g>u
inoremap ! !<c-g>u
inoremap ? ?<c-g>u

" Moving lines
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==
inoremap <C-k> <esc>:m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==

" Unbind
" nunmap <leader>wi
