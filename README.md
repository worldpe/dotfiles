Important programs:

yay -S paru-bin mako wofi alacritty fish mako wayland wl-clipboard gammastep wl-sunset grim slurp grimshot lf-bin mpv qutebrowser neofetch nvim qbittorrent qt5ct lxapperance weechat librewolf-bin freetube-bin xorg-wayland fd skim ripgrep fzf zathura exa ly fisher

paru -S musikcube adwaita-qt nerd-fonts-fira-code nerd-fonts-dejavu-complete gnome-themes-extra

Attempt 2:
sudo pacman -S alacritty cmake emacs ethtool exa fd firefox fish fisher fzf grim kitty wl-clipboard lxappearance micro mpc mpd mpv ncmpcpp neovim nicotine+ qt5ct qutebrowser skim slurp sway sxiv waybar zathura-pdf-mupdf ripgrep mako gammastep pipewire-pulse yay && yay -S paru && paru -S python-yams lf-bin cava nerd-fonts-iosevka freetube-bin grimshot ly ytfzf mpd-discord-rpc-git mpv-discordrpc
