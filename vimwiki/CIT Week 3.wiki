= Benchmarks =
1. Describe the five project management process groups, the typical level of activity for each, and the interactions among them
2. Relate the project management process groups to the project management knowledge areas
3. Discuss how organizations develop information technology (IT) project management methodologies to meet their needs
4. Review a case study of an organization applying the project management process groups to manage an IT project, describe outputs of each process group, and understand the contribution that effective initiating, planning, executing, monitoring and controlling, and closing make to project success
5. Review a case study of the same project managed with an agile focus and compare the key differences between an agile approach and a predictive approach
6. Describe several templates for creating documents for each process group

= Project Management Process Groups =
*Process:* 
* a series of actions directed towards a particular result

*Project management process groups:* 
* progress from initiating activities to planning acvitivies, executing activities, monitoring and controlling activities and closing activities.

*Initiating Processes:* 
* Include defining & authorizing a project or project phase
* Take place during each phase

*Planning Processes:*
* Include devising and maintaining a workable scheme to ensure that the project addresses the organization's needs
* Projects include several plans based upon various knowledge areas used by the project, such as schedule management, cost management, etc
* Plans may be revised during each phase of the project cycle

*Executing Processes:* 
* Include coordinating people & othe resources to carry out plans &create the product/services/results of projects or phases
* Examples: Directing & managing project work, managing project knowledge, acquiring resources & conducting procurements.

*Monitoring & Controlling processes:*
* Include regularly measuring & monitoring prgoress to ensure objectives are met
* Project/Staff leaders measure progress against plans and take corrective action when necessary
* Can include performance reports

*Closing Processes:*
* Include formalizing acceptance of the project or project phase & ending it efficiently
* Administrative activities are often involved, such as archiving project files, documenting lessons learned and receiving formal acceptance of the delivered work as part of the phase or project

* Process groups are not mutuall exclusive, and project managers must perform monitoring and controlling processes throughout the project's lifespan. 
* The level of activity and length of each process group varies for each project
* You can apply process groups for each major phase or iteration of a project or you can apply the process groups to the entire project.
* Spending more time on planning than other phases can lead to more successful projects, as less time and money is spent on execution later down the line.

= Mapping the Process Groups to the Knowledge Areas =
You can map the main activities into the 10 management knowledge areas:
*Project Management Process Groups:*
Project Integration Management:
  1. Develop project charter
  2. Develop project management plan
  3. Direct and manage project work
  4. Manage project knowledge
  5. Monitor and control project work
  6. Perform integrated change control
  7. 7. Close project or phase

= Developing an IT Project Management Methodology =
*PRINCE2(PRojects IN Controlled Environments:*
1. Starting up a project
2. Planning
3. Initiating a project
4. Directing a project
5. Controlling a stage
6. Managing product delivery
7. Managing stage boundaries
8. Closing a project

*Rational Unified Process Framework:*
* iterative software development process
* focuses on team productivity
* Has an open source version called OpenUP (You can license arbitrary development methods???)

*Six Sigma Methodologies:*
* Two main methods: 
* Define, Measure, Analyze, Improve, Control (DMAIC): Improve existing business process
* Define, Measure, Design, Verify (DMADV): Create a new product or process designs to achieve predictable, defect-free performance

= Developing an IT Project Management Methodology =
Case study 1

== Project Pre-Initiation and Initiation ==
Initiating the right project is important
JWD Consulting is a consultation firm, so any projects it does should be in that interest. 
JWD decides to create intranet, in hopes it will reduce cost & increase profits

== Pre-Initiation Tasks ==
Lay the groundwork of a project before it officially starts:
1. Determine the scope, time & cost constraints for the project
2. Identify the project sponsor
3. Select the project manager
4. Develop a business case for a project
5. Meet with the project manager to review the process and expectations for managing the project
6. Determine if the project should be divided into two or more smaller projects

== Initiating ==
Identify all the project stakeholders & develop a project charter
Output a project charter & a stakeholder register
JWD had a team consisting of a full-time consultant, part-time consultant, two IT memebers & two representatives of their two largest clients
*Stakeholder Management:* 
* a technique that project managers can use to help understand and increase the support of stakeholders throughout the project
* Results can be documented in the stakeholder register or stakeholder management strategy, depending on how sensitive the information is.
* Example: If a powerful stakeholder likes to stay on top of things and make money, having short face-to-face meetings and focusing on achieving the financial benefits
* Example 2: If someone seems like they are looking for other work then you can show them how the project will help the company and look good on their resume. 

== Project Planning ==
Used to guide project execution, must be realistic & useful
* Input: Output
* Develop project management plan: project management plan
* Plan scope management: scope management plan, requirements management plan
* Collect requirements: requirements documentation, requirements traceability matrix
* Define scope: project scope statement, project documents update

Some of the most important planning documents:
* team charter, an output of project resource management planning
* project scope statement
* work breakdown structurem a key part of the scope baseline
* project schedule, a Gantt chart with all dependencies and resources entered
* list of prioritized risk (part of a risk register)

== Project Execution == 
Input: Direct and manage project work
Output:
1. Deliverables
2. Work performance data
3. Issue log
4. Change request
5. Project management plan updates
6. Project documents updates
7. Organizational process assets updates

== Project Monitoring and Controlling ==
Input: Monitor & control project work
Outputs:
* Work performance reports
* Change request
* Project management plan updates
* Project documents updates
In the project, external help was asked when necessary to complete task, due to the importance of the project

== Project Closing ==
It is important to formally close out a project & confirm with all people involed that the deliverables are complete.
Input: Close project or phase
Outputs:
* Project documents updates
* Final product, service or result transition
* Final report
* Organizational process assets updates

*Final project report table of contents*
1. Project Objectives
2. Summary of project results
3. Original and actual start and end dates
4. Original and actual budget
5. Project Assesment (Why did you do this project? What did you produce? Was the project a success? What went right and wrong on the project?)
6. Transition plan
7. Annual project benefits measurement approach

= Case Study 2: JWD Consulting’s Project Management Intranet Site Project (Agile Approach) =
* Projects with heavy constraints, inexperienced and dispersed teams, large risks, generally clear up-front requirements, and a fairly rigid completion date are best done using a predictive approach.
* Projects with less rigid constraints, experienced and preferably co-located teams, smaller risks, unclear requirements, and more flexible scheduling would be more compatible with an agile approach.

== Scrum Roles, Artifacts, and Ceremonies ==
*Product owner:* 
* The person responsible for the business value of the project 
* Responsible for deciding what work to do in what order, as documented in the product backlog.
* In the case given, Joe Fleming, the CEO and person who suggested the product, fits this role

*ScrumMaster:* 
* The Person who:
   1. Ensures team productivity
   2. Facilitates the daily Scrum
   3. Enables close cooperation across all roles & functions
   4. Removes barriers that prevent the team from being effective
* Have authority over the process, but not the people on the team
* Must be comfortable surrendering control to the product owner and team
* Some experts suggest that traditional project managers do not make great ScrumMasters

*Scrum team or development team:*
* Cross-functional team of five-nine people who organize themselves and work to produce teh desired results for each sprint
* *Sprint:* normally last two to four weeks, during which specifc work must be completed and made ready to review
* Large projects might require teams of teams

=== Scrum ===
*Artifact*: 
* useful object created by people. 
* An artifact can be called a deliverable in other project management approaches.
*Examples of artifacts:*
* *Product backlog:* 
  * A list of features prioritized by business value.
  * The highest-priority items should be broken down in enough detail for the team to estimate the effort involved in developing them.
  * Some experts suggest scheduling about 10 workdays for each item. 
  * Size and complexity of the work dictates the estimate.
  * *Sprint Backlog:*
    * The highest-priority items from the product backlog to be completed within a sprint.
    * The Scrum team breaks down the highest-priority items into smaller tasks that take about 12 to 16 hours to complete.
    * Examples of a sprint backlog and product backlog are provided later in this section under Planning.
* *Burndown chart:*
  * Shows the cumulative work remaining in a sprint on a day-by-day basis. 
  * An example burndown chart is provided later in this section under Monitoring and Controlling.

*The Scrum Master faciliates four different ceremonies or meetings:*
* *Sprint planning session:* A meeting with the team to select a set of work from the product backlog to deliver during a sprint. This meeting takes about four hours to a full day.
* *Daily Scrum:* 
  * A short meeting for the development team to share progress and challenges and plan work for the day.
  * Ideally the team members are in the same place, the meeting usually lasts no more than 15 minutes, and it is held at the same time and place each day.
  * If that is not possible, teams can use videoconferencing to have short virtual meetings. 
  * The ScrumMaster asks what work has been done since yesterday, what work is planned for today, and what impediments or stumbling blocks might hamper the team’s efforts.
  * The ScrumMaster documents these stumbling blocks and works with key stakeholders to resolve them after the daily Scrum.
  * Many teams use the term issues for items that do not have to be solved in the next 24 hours and blockers for items that need to be addressed immediately.
  * This allows a ScrumMaster to maintain focus on highest-priority items (blockers) first and then manage the resolution of other issues over the next day or so.
* *Sprint Reviews:* meeting in which the team demonstrates to the product owner what it has completed during the sprint.
* *Sprint Retrospective:* meeting in which the team looks for ways to improve the product and the process based on a review of the actual performance of the development team.

== Project Pre-Initiation and Initiation ==
I guess get some ideas and stuff from people

== Planning ==
*Traditional planning:* Team charter, WBS, Grantt chart, list of prioritized risk, preliminary scope statement
*Scrum method:* Doesn't have this because the team will work as a self-directed group, coached by the ScrumMaster, making a team charter unnecessary
* In place of a WBS, high-level descriptions of the work to be completed would be identified in the product and sprint backlogs. 

== Executing ==
== Monitoring and Controlling ==
== Closing ==
= Templates by Process Group =
= Chapter Review =

I tapped out at the end

*Process:* a series of actions directed towards a particular result
*Executing:* processes include coordinating people and other resources to carry out project plans and create the products, services, or results of the project or phase
*PRINCE2:* What methodology was developed in the United Kingdom, defines 45 separate subprocesses, and organizes them into eight process groups?
*Business Case:* Which of the following outputs is often completed before initiating a project?
A work breakdown structure, project schedule, and cost estimates are outputs of the *planning* process.
Initiating involves developing a project charter, which is part of the project *integration* management knowledge area.
*Monitoring and controlling* involves measuring progress toward project objectives and taking corrective actions.
*The project has unclear up-front requirements*, you would generally use an agile approach.
Many people use *templates* to have a standard format for preparing various project management documents.

