= Data Models =
Data Redundancy: the same piece of data in multiple fields
Data Inconsistency: When redundant data has different values
Atomic Data: Data that cannot be broken down into smaller parts
  To avoid this, break up data values into multiple fields
Always calculate instead of storing calculated results
Pooling data avoids rekeying
Spreadsheet, database, model
  Spreadsheet = tab, row, column
  Database = table, record, field
  Model = Entity, Attribute
You use databases over spreadsheets to avoid redundant data

= Types of Database Distribution =
Centralized Systems
Distributed Database Systems
Homogenous Distributed Database Systems: Using the same software at multiple sites
Heterogenous Distributed Database Systems: Using different software are multiple sites

Relation: Links between tables
Cartesian Products: Every row of table 1 matched with every row of table 2

= Install =
MS Access
TOAD data modeler

= Database units =
Tables: What stores data
Forms: Allows for data to be inserted
Queries: Let you search for and compile data from multiple different tables
Reports: Allows you to present data

